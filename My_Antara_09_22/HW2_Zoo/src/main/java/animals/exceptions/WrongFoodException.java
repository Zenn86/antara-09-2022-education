package animals.exceptions;

public class WrongFoodException extends Exception{
    public WrongFoodException(String message) {
        System.out.println(message);
    }
}
