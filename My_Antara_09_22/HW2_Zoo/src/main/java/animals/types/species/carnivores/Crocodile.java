package animals.types.species.carnivores;

import animals.interfaces.Swim;
import animals.types.species.AbstractCarnivore;

public class Crocodile extends AbstractCarnivore implements Swim {

    public Crocodile() {
        super();
        setSpecie("The Crocodile");
        setName(getName() + " " + getSpecie());
        setSize(3);
    }

    public Crocodile(String name, int weight ) {
        super(name, weight);
        setSpecie("The Crocodile");
        setName(getName() + " " + getSpecie());
        setSize(3);
    }

    @Override
    public void swim() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Crocodile input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }

}
