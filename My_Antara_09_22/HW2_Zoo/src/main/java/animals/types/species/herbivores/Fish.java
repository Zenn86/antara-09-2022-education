package animals.types.species.herbivores;

import animals.interfaces.Swim;
import animals.types.species.AbstractHerbivore;

public class Fish extends AbstractHerbivore implements Swim {

    public Fish() {
        super();
        setSpecie("The Fish");
        setName(getName() + " " + getSpecie());
        setSize(1);
    }

    public Fish(String name, int weight ) {
        super(name, weight);
        setSpecie("The Fish");
        setName(getName() + " " + getSpecie());
        setSize(1);
    }

    @Override
    public void swim() {
        setSatiety(-1);
        setWeight(-1);
        System.out.print(getSpecie() + swim + getStatus());
        System.out.println(getStatus());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Fish input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }
}
