package animals.types.species.herbivores;

import animals.interfaces.Fly;
import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import animals.types.species.AbstractHerbivore;

public class Duck extends AbstractHerbivore implements Fly, Run, Swim, Voice {
    private final String voice = "Quack!";

    public Duck() {
        super();
        setSpecie("The Duck");
        setName(getName() + " " + getSpecie());
        setSize(2);
    }

    public Duck(String name, int weight) {
        super(name, weight);
        setSpecie("The Duck");
        setName(getName() + " " + getSpecie());
        setSize(2);
    }

    @Override
    public void fly() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + fly);
        System.out.println(getStatus());
    }

    @Override
    public void run() {
        setSatiety(-3);
        setWeight(-1);
        System.out.print(getSpecie() + run);
        System.out.println(getStatus());
    }

    @Override
    public void swim() {
        setSatiety(-1);
        setWeight(0);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Duck input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }
}
