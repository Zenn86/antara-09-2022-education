package animals.types.species.carnivores;

import animals.interfaces.Fly;
import animals.interfaces.Voice;
import animals.types.species.AbstractCarnivore;

public class Eagle extends AbstractCarnivore implements Fly, Voice {
    private final String voice = "Eagle sound!";

    public Eagle() {
        super();
        setSpecie("The Eagle");
        setName(getName() + " " + getSpecie());
        setSize(2);
    }

    public Eagle(String name, int weight ) {
        super(name, weight);
        setSpecie("The Eagle");
        setName(getName() + " " + getSpecie());
        setSize(2);
    }

    @Override
    public void fly() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + fly);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Eagle input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }
}
