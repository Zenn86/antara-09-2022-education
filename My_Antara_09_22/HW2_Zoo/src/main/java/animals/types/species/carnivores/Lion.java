package animals.types.species.carnivores;

import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import animals.types.species.AbstractCarnivore;

public class Lion extends AbstractCarnivore implements Run, Swim, Voice {
    private final String voice = "Roar!";

    public Lion() {
        super();
        setSpecie("The Lion");
        setName(getName() + " " + getSpecie());
        setSize(3);
    }

    public Lion(String name, int weight ) {
        super(name, weight);
        setSpecie("The Lion");
        setName(getName() + " " + getSpecie());
        setSize(3);
    }

    @Override
    public void run() {
        setSatiety(-2);
        setWeight(-1);
        System.out.print(getSpecie() + run);
        System.out.println(getStatus());
    }

    @Override
    public void swim() {
        setSatiety(-4);
        setWeight(-2);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Lion input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }
}
