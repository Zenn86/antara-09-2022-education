package animals.types.species;

import animals.AbstractAnimal;
import animals.exceptions.WrongFoodException;
import food.AbstractFood;
import food.products.AbstractMeat;

public abstract class AbstractHerbivore extends AbstractAnimal {
    public AbstractHerbivore() {
        super();
    }

    public AbstractHerbivore(String name, int weight) {
        super(name, weight);
    }

    @Override
    public void eat(AbstractFood food) throws WrongFoodException {
        if (food instanceof AbstractMeat) {
            throw new WrongFoodException("This animal is a Herbivore. Give him some grass!");
        }
        setSatiety(food.getEnergy() / 100);
        setWeight(food.getWeight());
        System.out.println(getName() + " has eaten a " + food.getName() + getStatus());
    }
}
