package animals.types.species;

import animals.AbstractAnimal;
import animals.exceptions.WrongFoodException;
import food.AbstractFood;
import food.products.AbstractGreen;

public abstract class AbstractCarnivore extends AbstractAnimal {

    public AbstractCarnivore() {
        super();
    }

    public AbstractCarnivore(String name, int weight) {
        super(name, weight);
    }

    @Override
    public void eat(AbstractFood food) throws WrongFoodException {
        if (food instanceof AbstractGreen) {
            throw new WrongFoodException("This animal is a Carnivore. Give him something meaty!");
        }
        this.setSatiety(food.getEnergy() / 100);
        setWeight(food.getWeight());
        System.out.println(getName() + getStatus());
    }

}
