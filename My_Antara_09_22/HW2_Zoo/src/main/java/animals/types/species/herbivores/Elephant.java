package animals.types.species.herbivores;

import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;
import animals.types.species.AbstractHerbivore;

public class Elephant extends AbstractHerbivore implements Run, Swim, Voice {
    private final String voice = "Elephant sound!";

    public Elephant() {
        super();
        setSpecie("The Elephant");
        setName(getName() + " " + getSpecie());
        setSize(4);
    }

    public Elephant(String name, int weight ) {
        super(name, weight);
        setSpecie("The Elephant");
        setName(getName() + " " + getSpecie());
        setSize(4);
    }

    @Override
    public void run() {
        setSatiety(-4);
        setWeight(-5);
        System.out.print(getSpecie() + run);
        System.out.println(getStatus());
    }

    @Override
    public void swim() {
        setSatiety(-3);
        setWeight(-3);
        System.out.print(getSpecie() + swim);
        System.out.println(getStatus());
    }

    @Override
    public String voice() {
        return voice;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj instanceof Elephant input) {
            return (input.getName().equals(getName()) && input.getStatus().equals(getStatus()));
        }
        return false;
    }
}
