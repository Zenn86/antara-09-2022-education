package animals;

import animals.exceptions.WrongFoodException;
import food.AbstractFood;

import java.util.Objects;

public abstract class AbstractAnimal {

    private int satiety;
    private int weight;
    private int size;
    private String name;
    private String specie;
    private String status = ". Current satiety: " + getSatiety() + ". Current weight: " + getWeight() + ".";

    public AbstractAnimal() {
        setName("John Doe");
        setWeight(10 + (int) (Math.random() * 300));
        setSatiety(10);
    }

    public AbstractAnimal(String name, int weight) throws IllegalArgumentException{
        if (weight <= 0) {
            throw new IllegalArgumentException();
        }
        setName(name);
        setWeight(weight);
        setSatiety(10);
    }

    public abstract void eat(AbstractFood food) throws WrongFoodException;

    public int getSatiety() {
        return satiety;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public String getSpecie() {
        return specie;
    }
    public int getSize() { return size; }

    protected void setSatiety(int inputSatiety) {
        this.satiety += inputSatiety;
        setStatus();
    }

    protected void setWeight(int inputWeight) {
        this.weight += inputWeight;
        setStatus();
    }

    protected void setName(String name) {
        this.name = name;
    }

    private void setStatus() {
        this.status = ". Current satiety: " + getSatiety() + ". Current weight: " + getWeight() + ".";
    }

    protected void setSpecie(final String specie) {
        this.specie = specie;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getStatus());
    }
}
