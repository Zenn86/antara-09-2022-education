import animals.interfaces.Swim;
import animals.types.species.carnivores.Crocodile;
import animals.types.species.carnivores.Eagle;
import animals.types.species.carnivores.Lion;
import animals.types.species.herbivores.Duck;
import animals.types.species.herbivores.Elephant;
import animals.types.species.herbivores.Fish;
import food.products.AbstractGreen;
import food.products.AbstractMeat;
import food.products.green.Apple;
import food.products.green.Carrot;
import food.products.green.Grass;
import food.products.meat.Beef;
import food.products.meat.Chicken;
import food.products.meat.Pork;
import worker.Worker;

public class Zoo {
    public static void main(String[] args) throws Exception {
        Worker worker1 = new Worker();

        Crocodile croc = new Crocodile("Crock", 700);
        Eagle eagley = new Eagle("Eagley", 7);
        Lion lyova = new Lion("Lyova", 200);
        Duck donald = new Duck("Donald", 4);
        Elephant dumbo = new Elephant("Dumbo", 5000);
        Fish nemo = new Fish("Nemo", 4);

        AbstractGreen apple = new Apple(1);
        AbstractGreen carrot = new Carrot(1);
        AbstractGreen grass = new Grass(50);
        AbstractMeat beef = new Beef(20);
        AbstractMeat chicken = new Chicken(2);
        AbstractMeat pork = new Pork(5);

        worker1.feed(croc, beef);
        croc.swim();
        worker1.feed(donald, pork);
        worker1.getVoice(eagley);
        worker1.feed(donald, carrot);
        worker1.getVoice(lyova);
        worker1.feed(lyova, grass);
        worker1.feed(dumbo, grass);
        dumbo.run();
        worker1.feed(nemo, apple);
        lyova.run();
        worker1.feed(lyova, chicken);

        Swim[] pound = new Swim[5];
        pound[0] = nemo;
        pound[1] = dumbo;
        pound[2] = donald;
        pound[3] = croc;
        pound[4] = lyova;
        for (Swim animal : pound) {
            animal.swim();
        }

    }
}
