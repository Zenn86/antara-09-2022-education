package worker;

import animals.AbstractAnimal;
import animals.exceptions.WrongFoodException;
import animals.interfaces.Voice;
import food.AbstractFood;

public class Worker {
    public void feed(AbstractAnimal animal, AbstractFood food) throws WrongFoodException {
        System.out.println("The Worker tried to feed " + animal.getName() + " with " + food.getName());
        animal.eat(food);
    }

    public void getVoice(Voice animal) {
        System.out.println("The worker forced the animal to speak.");
        System.out.println(animal.voice());
    }
}
