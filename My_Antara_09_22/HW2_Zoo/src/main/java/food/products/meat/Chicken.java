package food.products.meat;

import food.products.AbstractMeat;

public class Chicken extends AbstractMeat {
    public Chicken() {
        super();
        setEnergy(getWeight() * 200);
        setName("Chicken");
    }

    public Chicken(int weight) {
        super(weight);
        setEnergy(getWeight() * 200);
        setName("Chicken");
    }

}
