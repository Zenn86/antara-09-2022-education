package food.products.meat;

import food.products.AbstractMeat;

public class Pork extends AbstractMeat {
    public Pork() {
        super();
        setEnergy(getWeight() * 250);
        setName("Pork");
    }

    public Pork(int weight) {
        super(weight);
        setEnergy(getWeight() * 250);
        setName("Pork");
    }
}
