package food.products.green;

import food.products.AbstractGreen;

public class Carrot extends AbstractGreen {
    public Carrot() {
        super();
        setEnergy(getWeight() * 100);
        setName("Carrot");
    }

    public Carrot(int weight) {
        super(weight);
        setEnergy(getWeight() * 100);
        setName("Carrot");
    }
}
