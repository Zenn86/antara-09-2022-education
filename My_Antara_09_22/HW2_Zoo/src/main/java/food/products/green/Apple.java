package food.products.green;

import food.products.AbstractGreen;

public class Apple extends AbstractGreen {
    public Apple() {
        super();
        setEnergy(getWeight() * 150);
        setName("Apple");
    }

    public Apple(int weight) {
        super(weight);
        setEnergy(getWeight() * 150);
        setName("Apple");
    }
}
