package food.products.green;

import food.products.AbstractGreen;

public class Grass extends AbstractGreen {
    public Grass() {
        super();
        setEnergy(getWeight() * 80);
        setName("Grass");
    }

    public Grass(int weight) {
        super(weight);
        setEnergy(getWeight() * 80);
        setName("Grass");
    }
}
