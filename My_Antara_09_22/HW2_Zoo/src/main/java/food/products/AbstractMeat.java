package food.products;

import food.AbstractFood;

public abstract class AbstractMeat extends AbstractFood {
    public AbstractMeat() {
        super();
    }

    public AbstractMeat(int weight) {
        super(weight);
    }

}
