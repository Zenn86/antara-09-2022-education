package food;

public abstract class AbstractFood {
    private final int weight;
    private int energy;
    private String name;

    public AbstractFood() {
        this.weight = (1 + (int) (Math.random() * 10));
    }

    public AbstractFood(int weight) {
        this.weight = weight;
    }

    public int getEnergy() {
        return energy;
    }

    public int getWeight() {
        return weight;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

    protected void setEnergy(int energy) {
        this.energy = energy;
    }
}
