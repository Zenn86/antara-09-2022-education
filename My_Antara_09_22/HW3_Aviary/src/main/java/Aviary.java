import animals.AbstractAnimal;

import java.util.HashSet;

public class Aviary<T extends AbstractAnimal> {
    private final HashSet<T> aviary;
    private final Size aviarySize;

    public Aviary() {
        aviarySize = Size.SMALL;
        aviary = new HashSet<>();
    }

    public Aviary(Size sizeOfAviary) {
        aviarySize = sizeOfAviary;
        aviary = new HashSet<>();
    }

    public void addAnimal(T animal) throws IllegalArgumentException {
        if (animal.getSize() <= getAviarySize().getSize()) {
            aviary.add(animal);
        } else {
            throw new IllegalArgumentException("The animal is too big for this aviary");
        }
    }

    public void removeAnimal(T animal) {
        aviary.remove(animal);
    }

    public T getAnimal(String name) {
        for (T animal : aviary) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }

    public Size getAviarySize() {
        return aviarySize;
    }

}
