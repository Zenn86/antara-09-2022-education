import animals.types.species.AbstractCarnivore;
import animals.types.species.AbstractHerbivore;
import animals.types.species.carnivores.Crocodile;
import animals.types.species.carnivores.Eagle;
import animals.types.species.carnivores.Lion;
import animals.types.species.herbivores.Duck;
import animals.types.species.herbivores.Elephant;
import animals.types.species.herbivores.Fish;

public class Main {
    public static void main(String[] args) {
        Aviary<AbstractCarnivore> av1 = new Aviary<>(Size.LARGE);
        Aviary<AbstractHerbivore> av2 = new Aviary<>(Size.MEDIUM);
        Crocodile croc = new Crocodile("Crock", 700);
        Eagle eagley = new Eagle("Eagley", 7);
        Lion lyova = new Lion("Lyova", 200);
        Duck donald = new Duck("Donald", 4);
        Elephant dumbo = new Elephant("Dumbo", 5000);
        Fish nemo = new Fish("Nemo", 4);
        av1.addAnimal(lyova);
        av2.addAnimal(donald);
//        av1.addAnimal(donald); Ошибка
//        av2.addAnimal(lyova); Ошибка

        try {
            av2.addAnimal(dumbo);
        } catch (IllegalArgumentException exception) {
            System.out.println("The animal is too big for this aviary");
        }
        System.out.println(av1.getAnimal(lyova.getName()).getName());
        av1.addAnimal(croc);
        System.out.println(av1.getAnimal(croc.getName()).getName());
        av1.removeAnimal(croc);

        try {
            System.out.println(av1.getAnimal(croc.getName()).getName());
        } catch (NullPointerException exception) {
            System.out.println("Exception was thrown. It works");
        }
    }
}
