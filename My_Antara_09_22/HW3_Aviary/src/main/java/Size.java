public enum Size {
    SMALL ("Small", 1),
    MEDIUM ("Medium", 2),
    LARGE ("Large", 3),
    HUGE ("Huge", 4);

    private String title;
    private int size;

    Size(String title, int size) {
        this.title = title;
        this.size = size;
    }

    public String getTitle() {return title;}

    public int getSize() {
        return size;
    }
}
