-- 1. Вывести список самолетов с кодами 320, 321, 733
SELECT AIRCRAFT_CODE, MODEL FROM AIRCRAFTS_DATA ad
WHERE AIRCRAFT_CODE IN('320', '321', '733');

-- 2. Вывести список самолетов с кодом не на 3
SELECT AIRCRAFT_CODE, MODEL FROM AIRCRAFTS_DATA ad
WHERE AIRCRAFT_CODE NOT LIKE '3%';

-- 3. Найти билеты оформленные на имя «OLGA», с емайлом «OLGA» или без емайла
SELECT t.TICKET_NO, t.PASSENGER_NAME, t.EMAIL FROM TICKETS t 
WHERE UPPER(t.PASSENGER_NAME) LIKE '%OLGA%' AND (UPPER(t.EMAIL) LIKE '%OLGA%' OR t.EMAIL = NULL);

-- 4. Найти самолеты с дальностью полета 5600, 5700. Отсортировать список по убыванию дальности полета
SELECT ad."RANGE" , ad.MODEL FROM AIRCRAFTS_DATA ad
WHERE ad."RANGE" IN (5600, 5700)
ORDER BY ad."RANGE" DESC, ad.MODEL; 

-- 5. Найти аэропорты в Moscow. Вывести название аэропорта вместе с городом. Отсортировать по полученному названию
SELECT ad.AIRPORT_NAME, ad.CITY FROM AIRPORTS_DATA ad 
WHERE LOWER(ad.CITY) = LOWER('Moscow')
ORDER BY AIRPORT_NAME; 
-- если Вывести название аэропорта вместе с городом" понимать именно как конкатенациюб то такой вариант:
SELECT ad.AIRPORT_NAME || ' - ' || ad.CITY FROM AIRPORTS_DATA ad 
WHERE LOWER(ad.CITY) = LOWER('Moscow') 
ORDER BY AIRPORT_NAME;

-- 6. Вывести список всех городов без повторов в зоне «Europe»
SELECT DISTINCT ad.CITY FROM AIRPORTS_DATA ad 
WHERE LOWER(ad.TIMEZONE) LIKE 'europe%';

-- 7. Найти бронирование с кодом на «3A4» и вывести сумму брони со скидкой 10%
SELECT b.BOOK_REF, (b.TOTAL_AMOUNT * 0.9) FROM BOOKINGS b 
WHERE b.BOOK_REF LIKE '3A4%';

/* 8. Вывести все данные по местам в самолете с кодом 320 и классом «Business» строками вида «Данные по месту: номер места 1», 
 * «Данные по месту: номер места 2» … и тд*/
SELECT '«Seat info: seat number ' || s.SEAT_NO || '. Class: ' || s.FARE_CONDITIONS || '»' FROM SEATS s 
WHERE s.AIRCRAFT_CODE = '320' AND s.FARE_CONDITIONS = 'Business';

-- 9. Найти максимальную и минимальную сумму бронирования в 2017 году
SELECT MAX(b.TOTAL_AMOUNT), MIN(b.TOTAL_AMOUNT)  FROM BOOKINGS b
WHERE TRUNC(b.BOOK_DATE, 'YYYY') = TRUNC(TO_DATE('2017', 'YYYY'), 'YYYY');  

-- 10. Найти количество мест во всех самолетах, вывести в разрезе самолетов
SELECT ad.MODEL, COUNT(s.SEAT_NO) FROM AIRCRAFTS_DATA ad 
INNER JOIN SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE 
GROUP BY ad.MODEL 
ORDER BY ad.MODEL;

-- 11. Найти количество мест во всех самолетах с учетом типа места, вывести в разрезе самолетов и типа мест
SELECT ad.MODEL, s.FARE_CONDITIONS, COUNT(s.SEAT_NO) FROM AIRCRAFTS_DATA ad 
INNER JOIN SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE
GROUP BY ad.MODEL, s.FARE_CONDITIONS  
ORDER BY ad.MODEL;

-- 12. Найти количество билетов пассажира ALEKSANDR STEPANOV, телефон которого заканчивается на 11
SELECT t.PASSENGER_NAME, COUNT(*) FROM TICKETS t
WHERE t.PASSENGER_NAME LIKE 'ALEKSANDR STEPANOV' AND t.PHONE LIKE '%11'
GROUP BY t.PASSENGER_NAME; 

-- 13. Вывести всех пассажиров с именем ALEKSANDR, у которых количество билетов больше 2000. Отсортировать по убыванию количества билетов
SELECT t.PASSENGER_NAME, COUNT(*) FROM TICKETS t
WHERE t.PASSENGER_NAME LIKE '%ALEKSANDR%'
GROUP BY t.PASSENGER_NAME
HAVING COUNT(*) > 2000
ORDER BY COUNT(*) DESC;  

-- 14. Вывести дни в сентябре 2017 с количеством рейсов больше 500
SELECT TRUNC(DATE_DEPARTURE), COUNT(*)  FROM FLIGHTS f 
WHERE TRUNC(DATE_DEPARTURE, 'MM') = TRUNC(TO_DATE('09/2017', 'MM/YYYY'), 'MM') 
GROUP BY TRUNC(DATE_DEPARTURE)
HAVING COUNT(*) > 500
ORDER BY TRUNC(DATE_DEPARTURE);

-- 15. Вывести список городов, в которых несколько аэропортов
SELECT CITY, 'Qquantity of airports: ' || COUNT(*) Airports FROM AIRPORTS_DATA ad 
GROUP BY CITY 
HAVING COUNT(*) > 1 

-- 16. Вывести модель самолета и список мест в нем, т.е. на самолет одна строка результата
SELECT ad.MODEL, LISTAGG(s.SEAT_NO, ', ') WITHIN GROUP (ORDER BY s.SEAT_NO) "Seats" FROM AIRCRAFTS_DATA ad 
INNER JOIN SEATS s ON ad.AIRCRAFT_CODE = s.AIRCRAFT_CODE 
GROUP BY ad.MODEL 
ORDER BY ad.MODEL;

-- 17. Вывести информацию по всем рейсам из аэропортов в г.Москва за сентябрь 2017
SELECT * FROM FLIGHTS f 
INNER JOIN AIRPORTS_DATA ad ON f.DEPARTURE_AIRPORT = ad.AIRPORT_CODE 
WHERE LOWER(ad.CITY) = 'moscow' AND TRUNC(DATE_DEPARTURE , 'mm') = TRUNC(TO_DATE('09/2017', 'mm/yyyy'), 'mm')  
ORDER BY DATE_DEPARTURE; 

-- 18. Вывести кол-во рейсов по каждому аэропорту в г.Москва за 2017
SELECT ad.AIRPORT_NAME || '. Arrivals: ', COUNT(f.FLIGHT_NO)  FROM AIRPORTS_DATA ad
JOIN FLIGHTS f ON ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT 
WHERE TRUNC(f.DATE_DEPARTURE , 'YYYY') = TRUNC(TO_DATE('2017', 'YYYY'), 'YYYY') AND LOWER(ad.CITY) = 'moscow'
GROUP BY ad.AIRPORT_NAME  
UNION 
SELECT ad.AIRPORT_NAME || '. Departures: ', COUNT(f.FLIGHT_NO)  FROM AIRPORTS_DATA ad
JOIN FLIGHTS f ON ad.AIRPORT_CODE = f.ARRIVAL_AIRPORT  
WHERE TRUNC(f.DATE_ARRIVAL  , 'YYYY') = TRUNC(TO_DATE('2017', 'YYYY'), 'YYYY') AND LOWER(ad.CITY) = 'moscow'
GROUP BY ad.AIRPORT_NAME  
ORDER BY 1; 

-- 19. Вывести кол-во рейсов по каждому аэропорту, месяцу в г.Москва за 2017
SELECT ad.AIRPORT_NAME || '. Arrivals: ', TO_CHAR(f.DATE_DEPARTURE, 'yyyy/mm'), COUNT(f.FLIGHT_NO)  FROM AIRPORTS_DATA ad
JOIN FLIGHTS f ON ad.AIRPORT_CODE = f.DEPARTURE_AIRPORT 
WHERE TRUNC(f.DATE_DEPARTURE , 'YYYY') = TRUNC(TO_DATE('2017', 'YYYY'), 'YYYY') AND LOWER(ad.CITY) = 'moscow'
GROUP BY ad.AIRPORT_NAME, TO_CHAR(f.DATE_DEPARTURE, 'yyyy/mm')  
UNION 
SELECT ad.AIRPORT_NAME || '. Departures: ', TO_CHAR(f.DATE_ARRIVAL, 'yyyy/mm'), COUNT(f.FLIGHT_NO)  FROM AIRPORTS_DATA ad
JOIN FLIGHTS f ON ad.AIRPORT_CODE = f.ARRIVAL_AIRPORT  
WHERE TRUNC(f.DATE_ARRIVAL  , 'YYYY') = TRUNC(TO_DATE('2017', 'YYYY'), 'YYYY') AND LOWER(ad.CITY) = 'moscow'
GROUP BY ad.AIRPORT_NAME, TO_CHAR(f.DATE_ARRIVAL, 'yyyy/mm')  
ORDER BY 1; 
-- 20. Найти все билеты по бронированию на «3A4B»
SELECT * FROM TICKETS t 
WHERE BOOK_REF LIKE '3A4B%'
ORDER BY BOOK_REF; 
-- 21. Найти все перелеты по бронированию на «3A4B»
SELECT t.BOOK_REF , f.* FROM TICKET_FLIGHTS tf 
JOIN FLIGHTS f ON tf.FLIGHT_ID = f.FLIGHT_ID 
JOIN TICKETS t ON tf.TICKET_NO = t.TICKET_NO 
WHERE t.BOOK_REF LIKE '3A4B%'
ORDER BY t.BOOK_REF;
