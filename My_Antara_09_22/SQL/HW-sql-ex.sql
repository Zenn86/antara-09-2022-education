-- Задание: 1 (Serge I: 2002-09-30)
-- Найдите номер модели, скорость и размер жесткого диска для всех ПК стоимостью менее 500 дол. Вывести: model, speed и hd 
SELECT model, speed, hd from PC 
where price < 500
-- Задание: 2 (Serge I: 2002-09-21)
-- Найдите производителей принтеров. Вывести: maker 
Select distinct pr.maker from Product pr
where type = 'Printer'
-- Задание: 3 (Serge I: 2002-09-30)
-- Найдите номер модели, объем памяти и размеры экранов ПК-блокнотов, цена которых превышает 1000 дол. 
select model, ram, screen from laptop
where price > 1000
-- Задание: 4 (Serge I: 2002-09-21)
-- Найдите все записи таблицы Printer для цветных принтеров. 
Select * from Printer
where color = 'y'
-- Задание: 5 (Serge I: 2002-09-30)
-- Найдите номер модели, скорость и размер жесткого диска ПК, имеющих 12x или 24x CD и цену менее 600 дол. 
select model, speed, hd from PC
where (cd ='12x' OR cd ='24x') AND price < 600
-- Задание: 6 (Serge I: 2002-10-28)
-- Для каждого производителя, выпускающего ПК-блокноты c объёмом жесткого диска не менее 10 Гбайт, найти 
-- скорости таких ПК-блокнотов. Вывод: производитель, скорость. 
select distinct pr.maker, lp.speed from Product pr
inner join Laptop lp on pr.model = lp.model
where lp.hd > 9
-- Задание: 7 (Serge I: 2002-11-02)
-- Найдите номера моделей и цены всех имеющихся в продаже продуктов (любого типа) производителя B (латинская 
-- буква).
Select pr.model, price from Product pr
INNER JOIN PC on pr.model = PC.model
WHERE pr.maker = 'B'
UNION
Select pr.model, price from Product pr
INNER JOIN Laptop lp on pr.model = lp.model
WHERE pr.maker = 'B'
UNION
Select pr.model, price from Product pr
INNER JOIN Printer prt on pr.model = prt.model
WHERE pr.maker = 'B'
-- Задание: 8 (Serge I: 2003-02-03)
-- Найдите производителя, выпускающего ПК, но не ПК-блокноты. 
select pr.maker from Product pr
where type = 'PC'
EXCEPT
select pr.maker from Product pr
where type = 'Laptop'
-- Задание: 9 (Serge I: 2002-11-02)
-- Найдите производителей ПК с процессором не менее 450 Мгц. Вывести: Maker
select distinct maker from Product pr
INNER JOIN PC on pr.model = pc.model
where PC.speed >= 450
-- Задание: 10 (Serge I: 2002-09-23)
-- Найдите модели принтеров, имеющих самую высокую цену. Вывести: model, price 
select model, price from Printer
where price = (select MAX(price) from Printer)
-- Задание: 11 (Serge I: 2002-11-02)
-- Найдите среднюю скорость ПК.
select AVG(speed) from PC
-- Задание: 12 (Serge I: 2002-11-02)
-- Найдите среднюю скорость ПК-блокнотов, цена которых превышает 1000 дол. 
select AVG(speed) from Laptop
where price > 1000
-- Задание: 13 (Serge I: 2002-11-02)
-- Найдите среднюю скорость ПК, выпущенных производителем A. 
select AVG(speed) from PC
inner join Product pr on PC.model = pr.model
where pr.maker = 'A'
-- Задание: 14 (Serge I: 2002-11-05)
-- Найдите класс, имя и страну для кораблей из таблицы Ships, имеющих не менее 10 орудий. 
Select sh.class, sh.name, cl.country from Ships sh
INNER JOIN Classes cl on sh.class = cl.class
where cl.numGuns >= 10
-- Задание: 15 (Serge I: 2003-02-03)
-- Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD
select hd from PC
GROUP BY hd
HAVING count(*) >= 2
/* Задание: 16 (Serge I: 2003-02-03)
Найдите пары моделей PC, имеющих одинаковые скорость и RAM. В результате каждая пара указывается только
 один раз, т.е. (i,j), но не (j,i), Порядок вывода: модель с большим номером, модель с меньшим номером,
 скорость и RAM. */
select distinct pc1.model, pc2.model, pc1.speed, pc1.ram from PC pc1, PC pc2
where pc1.speed = pc2.speed AND pc1.ram = pc2.ram AND pc1.model > pc2.model
-- Задание: 17 (Serge I: 2003-02-03)
-- Найдите модели ПК-блокнотов, скорость которых меньше скорости каждого из ПК.
-- Вывести: type, model, speed
select distinct pr.type, lp.model, lp.speed from Product pr
INNER JOIN Laptop lp on pr.model = lp.model
where lp.speed < ALL(select speed from PC)
-- Задание: 18 (Serge I: 2003-02-03)
-- Найдите производителей самых дешевых цветных принтеров. Вывести: maker, price 
select prod.maker, prin.price from Product prod
INNER JOIN Printer prin on prod.model = prin.model
where color = 'y' 
group by maker, price
having price = (Select MIN(price) from Printer
                 where color = 'y')
-- Задание: 19 (Serge I: 2003-02-13)
-- Для каждого производителя, имеющего модели в таблице Laptop, найдите средний размер экрана выпускаемых им
-- ПК-блокнотов.
-- Вывести: maker, средний размер экрана. 
select distinct maker, AVG(screen) from Product pr
INNER JOIN Laptop lp on pr.model = lp.model
group by maker
-- Задание: 20 (Serge I: 2003-02-13)
-- Найдите производителей, выпускающих по меньшей мере три различных модели ПК. Вывести: Maker, число 
-- моделей ПК. 
select distinct maker, count(model) from Product
where type = 'PC'
group by maker
having count(model) > 2
-- Задание: 21 (Serge I: 2003-02-13)
-- Найдите максимальную цену ПК, выпускаемых каждым производителем, у которого есть модели в таблице PC.
-- Вывести: maker, максимальная цена. 
select maker, MAX(price) from Product pr
INNER JOIN PC on pr.model = PC.model
group by maker



