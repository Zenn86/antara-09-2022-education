import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik(10, "Fluffer", 5, "meow");
        Kotik kotik2 = new Kotik();
        kotik2.setKotik(3, "Destroyer", 10, "MEOW");
        kotik2.liveAnotherDay();
        System.out.println("The cat's name: " + kotik2.getName() + ". The cat's weight: " + kotik2.getWeight());
        if (kotik1.getMeow().equals(kotik2.getMeow())) {
            System.out.println("These two cats have equal voices!");
        } else {
            System.out.println("These two cats don't have equal voices! kotik1 says: " + kotik1.getMeow() + "! And kotik2 says: " + kotik2.getMeow() + "!");
        }
        System.out.println("Quantity of cats: " + Kotik.getQuantityOfCats());
    }
}
